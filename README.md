# Antre de guerre

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/antre-de-guerre.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/antre-de-guerre.git
```
