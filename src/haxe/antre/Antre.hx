package antre;

import patchman.module.GameEvents;
import merlin.value.MerlinFloat;
import merlin.value.MerlinValue;
import merlin.Merlin;
import patchman.Ref;
import patchman.IPatch;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import merlin.IAction;

@:build(patchman.Build.di())
class Antre {
    public var deaths: Int;
    public var posDeaths: Int;
    public var posIcon: Int;
    public var numberOfDeaths: Int;
    public var numberOfDeathsSprite: Dynamic;
    public var deathBar: Dynamic;
    private var gameEvents: GameEvents;

    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    private function saveData(game: hf.mode.GameMode): Void {
        var maxLevel: Int = game.dimensions[0].currentId;
        var itemsTaken: Array<Int> = game.getPicks2();
        var items: Map<String, Int> = new Map();
        for (i in 0...itemsTaken.length) {
            if (itemsTaken[i] != null) {
                items.set(Std.string(i), itemsTaken[i]);
            }
        }
        this.gameEvents.endGame(new ef.api.run.SetRunResultOptions(maxLevel == 25, maxLevel, game.savedScores, items, {
            var obj = {};
            Obfu.setField(obj, "deaths", this.numberOfDeaths);
            obj;
        }));
    }



    public function new(gameEvents: GameEvents) {
        this.gameEvents = gameEvents;

        var patches = [
            Ref.auto(hf.entity.Player.killPlayer).before(function(hf: hf.Hf, self: hf.entity.Player): Void {
                this.numberOfDeaths += 1;
                if (self.lives <= 0)
                    self.lives = 1;
            }),

            Ref.auto(hf.gui.GameInterface.setLives).replace(function(hf: hf.Hf, self: hf.gui.GameInterface, pid: Int, v: Int): Void {
                if (this.numberOfDeaths <= this.deaths)
                    Merlin.setGlobalVar(self.game, Obfu.raw("REMAINING_DEATHS"), new MerlinFloat(this.deaths - this.numberOfDeaths));
                self.update();
            }),

            Ref.auto(hf.mode.GameMode.onMap).wrap(function(hf: hf.Hf, self: hf.mode.GameMode, old): Void {
                if (this.numberOfDeaths > 50)
                    self.onPause();
                else
                    old(self);
            }),

            Ref.auto(hf.mode.Adventure.saveScore).replace(function(hf: hf.Hf, self: hf.mode.Adventure): Void {
                this.saveData(self);
            }),

            Ref.auto(hf.mode.MultiCoop.saveScore).replace(function(hf: hf.Hf, self: hf.mode.MultiCoop): Void {
                this.saveData(self);
            })
        ];
        this.patches = FrozenArray.from(patches);
    }
}
