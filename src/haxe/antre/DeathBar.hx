package antre;

import merlin.value.MerlinFloat;
import merlin.Merlin;
import patchman.IPatch;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import bottom_bar.BottomBarInterface;
import bottom_bar.modules.BottomBarModule;

class DeathBar extends BottomBarModule {
    public var antre: Antre;
    public var x: Int;
    public var deaths: Int;

    public var patches(default, null): FrozenArray<IPatch>;

    public function new(module: Map<String, Dynamic>, antre: Antre) {
        this.antre = antre;
        this.x = module[Obfu.raw("x")];
        this.deaths = module[Obfu.raw("deaths")];
        this.antre.deaths = this.deaths;
    }

    public override function init(bottomBar:BottomBarInterface): Void {
        this.antre.posDeaths = this.x;
        this.antre.posIcon = this.x + 42;

        this.antre.numberOfDeaths = 0;
        this.antre.numberOfDeathsSprite = bottomBar.game.depthMan.attach('hammer_interf_inGameMsg', bottomBar.game.root.Data.DP_TOP + 1);
        this.antre.numberOfDeathsSprite.field._visible = true;
        this.antre.numberOfDeathsSprite.field._width = 400;
        this.antre.numberOfDeathsSprite.field._xscale = 115;
        this.antre.numberOfDeathsSprite.field._yscale = 115;
        this.antre.numberOfDeathsSprite.field._x = 0;
        this.antre.numberOfDeathsSprite.field._y = 499;
        this.antre.numberOfDeathsSprite.label._visible = false;
        bottomBar.game.root.FxManager.addGlow(this.antre.numberOfDeathsSprite, 7366029, 2);

        var deathIcon = bottomBar.game.root.Std.attachMC(bottomBar.mc, 'hammer_interf_ninjaIcon', bottomBar.game.manager.uniq++);
        // var deathIcon = bottomBar.game.depthMan.attach('hammer_interf_ninjaIcon', bottomBar.game.root.Data.DP_TOP + 1);
        deathIcon._x = this.antre.posIcon;
        deathIcon._y = -3;
        deathIcon.stop();

        Merlin.setGlobalVar(bottomBar.game, Obfu.raw("REMAINING_DEATHS"), new MerlinFloat(this.deaths));

        var deathBarContainer = bottomBar.game.depthMan.empty(bottomBar.game.root.Data.DP_TOP);
        deathBarContainer.beginFill(0);
        deathBarContainer._alpha = 100;
        deathBarContainer.moveTo(this.antre.posIcon + 9, 504);
        deathBarContainer.lineTo(this.antre.posIcon + 9, 516);
        deathBarContainer.lineTo(this.antre.posIcon + 111, 516);
        deathBarContainer.lineTo(this.antre.posIcon + 111, 504);
        deathBarContainer.lineTo(this.antre.posIcon + 9, 504);
        deathBarContainer.endFill();

        this.antre.deathBar = bottomBar.game.depthMan.empty(bottomBar.game.root.Data.DP_TOP);
        this.antre.deathBar.beginFill(2948927);
        this.antre.deathBar._alpha = 100;
        this.antre.deathBar.moveTo(this.antre.posIcon + 10, 505);
        this.antre.deathBar.lineTo(this.antre.posIcon + 10, 515);
        this.antre.deathBar.lineTo(this.antre.posIcon + 110, 515);
        this.antre.deathBar.lineTo(this.antre.posIcon + 110, 505);
        this.antre.deathBar.lineTo(this.antre.posIcon + 10, 505);
        this.antre.deathBar.endFill();

        this.update(bottomBar);
    }

    public override function update(bottomBar: BottomBarInterface): Bool {
        if (this.antre.numberOfDeaths != this.antre.numberOfDeathsSprite.field.text) {
            this.antre.numberOfDeathsSprite.field.text = this.antre.numberOfDeaths;
            this.antre.numberOfDeathsSprite.field._x = this.antre.posDeaths - 5 * (this.antre.numberOfDeaths > 9 ? 1 : 0) - 5 * (this.antre.numberOfDeaths > 99 ? 1 : 0);

            if (this.antre.numberOfDeaths <= this.deaths) {
                var size = (100.0 * (this.deaths - this.antre.numberOfDeaths)) / this.deaths;
                this.antre.deathBar.removeMovieClip();
                this.antre.deathBar = bottomBar.game.depthMan.empty(bottomBar.game.root.Data.DP_TOP);
                this.antre.deathBar.beginFill(size > 50 ? 2617145 : (size > 20 ? 15920640 : 16272133));
                this.antre.deathBar._alpha = 100;
                this.antre.deathBar.moveTo(this.antre.posIcon + 10, 505);
                this.antre.deathBar.lineTo(this.antre.posIcon + 10, 515);
                this.antre.deathBar.lineTo(this.antre.posIcon + 10 + size, 515);
                this.antre.deathBar.lineTo(this.antre.posIcon + 10 + size, 505);
                this.antre.deathBar.lineTo(this.antre.posIcon + 10, 505);
                this.antre.deathBar.endFill();
            }
        }
        return true;
    }
}
