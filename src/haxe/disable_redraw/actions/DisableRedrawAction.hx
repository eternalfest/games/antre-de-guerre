package disable_redraw.actions;

import etwin.Obfu;
import merlin.IActionContext;
import merlin.IAction;

// disableRedraw(force=false);
// disableRedraw(force=true);
// disableRedraw();

class DisableRedrawAction implements IAction {
  public var name(default, null): String = Obfu.raw("disableRedraw");
  public var isVerbose(default, null): Bool = false;

  private var mod: DisableRedraw;

  public function new(mod: DisableRedraw) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: hf.mode.GameMode = ctx.getGame();

    game.world.scriptEngine.fl_redraw = false;

    return false;
  }
}
