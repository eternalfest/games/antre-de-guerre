package disable_redraw;

import disable_redraw.actions.DisableRedrawAction;
import merlin.IAction;

@:build(patchman.Build.di())
class DisableRedraw {
  @:diExport
  public var action(default, null): IAction;

  public function new() {
    this.action = new DisableRedrawAction(this);
  }
}
