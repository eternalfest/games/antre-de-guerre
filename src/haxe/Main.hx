import etwin.Obfu;
import maxmods.AddLink;
import bottom_bar.BottomBar;
import bottom_bar.modules.BottomBarModuleFactory;
import disable_redraw.DisableRedraw;
import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import patchman.IPatch;
import patchman.Patchman;
import better_script.NoNextLevel;
import antre.Antre;
import antre.DeathBar;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
      bugfix: Bugfix,
      debug: Debug,
      gameParams: GameParams,
      noNextLevel: NoNextLevel,
      bottomBar: BottomBar,
      addLink: AddLink,
      disableRedraw: DisableRedraw,
      antre: Antre,
      merlin: Merlin,
      patches: Array<IPatch>,
      hf: Hf
  ) {
      BottomBarModuleFactory.get().addModule(Obfu.raw("DeathBar"), function(data: Dynamic) return new DeathBar(data, antre));
    Patchman.patchAll(patches, hf);
  }
}
